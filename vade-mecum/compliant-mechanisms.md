# Compliant Mechanisms & Flexures

## Definitions

[Compliant mechanism](https://en.m.wikipedia.org/wiki/Compliant_mechanism)

Monolitics structure that transform a mechanical input into a desired mechanical output using hinges.

[Mechanical Metamaterials](

## Design methods

* kinematics approach
   * Pseudo-rigid body model
* structural optimization approach
  * FEM
* [Design engine of Disney](https://www.engineering.com/story/disney-research-introduces-design-tool-for-compliant-mechanisms)

## Constraints and Degree Of Freedom

* **Pin flexure** - 3 dof : 2 translations, 1 rotation
* **Blade flexure** - twist is easier when the beam is thinner - 4 dof are constrained
* **Notch flexure** - no twist - 5 dof are constrained - 1 dof (twist is supressed)

## building blocks

Pinned vs clamped beam

![](videos/pinned-clamped-beam.mp4)

rotation

![](videos/rotation-crossed-beams.mp4)

## mechanisms

roration

![](videos/rotation.mp4)

Elephant

![Elephant](videos/WYRD-elephant.mp4)

Vise-grip

![Gripper](videos/grip-solidhinges.mp4)

![Gripper](videos/grip-flex.mp4)

## Bistable Mechanisms

![](videos/bistable-beam.mp4)

![](videos/bistable-4-bars.mp4)

![](videos/switch.mp4)

## Can we 3D print these mechanisms ?

Yes, but ...

### [3D printing material](https://www.simplify3d.com/support/materials-guide/)

* [stress-strain curve](https://en.wikipedia.org/wiki/Stress%E2%80%93strain_curve)
* [Elasticity]()
* [Fatigue life](https://en.m.wikipedia.org/wiki/Fatigue_(material))

## Precision

[Lego](https://patents.google.com/patent/US3005282A/en)

In a well designed and preloaded elastically averaged coupling, the repeatability is approximately inversely proportional to the square root of the number  of contact points  [Alexander Slocum].

## Applications

Turn multi components objects into monolitics object.

  - truster orienter
  - deployable solar panels

### Prosthetics

* Possible applications
* [Jaipurfoot](https://www.jaipurfoot.org/)

### Computing

* [Micro-mechanical logic gates](https://www.nature.com/articles/s41467-019-08678-0)
* [Mechanical computing systems Using Only Links and Rotary Joints (github)](https://github.com/mattmoses/MechanicalComputingSystems)

### Robotics

* Flexures and compliant mechanisms have change the field of robotics

## Ressources

* [Veritassium](https://www.youtube.com/watch?v=97t7Xj_iBv0)
* [The FACTs of Mechanical Design, educational YouTube channel](https://www.youtube.com/channel/UC5Jz6SBlu2Sv61kfssv4DOw)
* [BioE271: Frugal Science, Stanford University, USA](https://www.frugalscience.org/lectures)
* [BYU CMR](https://www.compliantmechanisms.byu.edu/maker-resources)
* [FlexLinks, CMR, BYU](https://www.compliantmechanisms.byu.edu/flexlinks)
* [Mecanical Metamaterial FabLab ULB workshop](https://fablab-ulb.gitlab.io/projects/2020-fabxlive/workshop-mechanical-metamaterials/)
* [Flexures blog by mact](https://web.mit.edu/mact/www/Blog/Flexures/FlexureIndex.html)
* The Handbook of Compliant Mechanisms
* [FACT practice](https://www.sciencedirect.com/science/article/pii/S0141635909000932?casa_token=CYYpZweQCJYAAAAA:nYXqcCIxWWmUsfMFwn1PKkEeAuvSPTLC6wlpshCLp2bAeh5skuMOzk1VySiTtFQk6LNvlHRx9jE)
