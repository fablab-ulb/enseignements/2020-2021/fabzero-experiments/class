# OpenSCAD

OpenSCAD is a code-based software on which you describe your 3D design by coding it.

## Learn OpenSCAD

The easiest way to learn OpenSCAD is starting from the [OpenSCAD cheat sheet](https://www.openscad.org/cheatsheet/) and test simple forms.

## 3D basics

A simple cube : 


```
cube(10); //a 10mmx10mmx10mm cube
```

A simple "cube" : 

```
cube([10,20,5]); //a 10mmx20mmx05mm "cube"
```

An even better coded cube : 

```
side = 10;
cube([side,2*side,side/2]); //a 10mmx20mmx5mm parametric cube
```

Other very simple examples : see the cheat sheet.

## Operators

Union (actually default) : 

```
side=10;
union(){
    cube(side, center=true); 
    sphere(r=side/sqrt(2));
}
```

Difference : 

```
difference(){
    side=10;
    cube(side, center=true); 
    sphere(r=side/sqrt(2));
}
```

Hull : 

```
hull(){
    side=10;
    cube(side, center=true);
    sphere(r=side/sqrt(2));
}
```

Minkowski :

```
minkowski(){
    side=10;
    cube(side, center=true);
    sphere(r=side/sqrt(2));
}
```

## Transformations

Rotation

```
rotate([45,45,0])cube(10);
```

Translation

```
translate([0,0,10])cube(10);
```

Combinations

```
rotate([45,45,0])
translate([0,0,10])
    cube(10);
```

## Animate

```
rotate([0,0,$t*360])
translate([20,0,0])
    cube(10);
```


## Re-use! (and share...)

- Thingiverse

![](img/thingiverse_customizable.mp4)

## Pro-tips 

- **indent your code**
- think **simple** : better several operations than incomprehensible multimatrix
- write everything as **parametric code** directly (you do the maths)
- split things in modules
- (use "e=0.01" to avoid artifacts)
